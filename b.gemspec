require_relative "lib/b/version"

Gem::Specification.new do |spec|
  spec.name        = "b"
  spec.version     = B::VERSION
  spec.authors     = ["Vladimir Támara Patiño"]
  spec.email       = ["vtamara@pasosdeJesus.org"]
  spec.homepage    = "https://example.com"
  spec.summary     = "T"
  spec.description = "T"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "http://example.com"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://example.com"
  spec.metadata["changelog_uri"] = "https://example.com"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0"
end
