Rails.application.routes.draw do
  scope "/a" do
    resources :users
    mount B::Engine => "/", as: :b
  end

end
